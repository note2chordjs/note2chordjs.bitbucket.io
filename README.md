# note2chord.js v0.13
## 概要
ノートの配列を渡すとコードネームの候補を返す関数が入っているライブラリーです。

### 特徴
- スラッシュコード(C7/G など)に対応
- 度数表記(Ⅲm7 など)も可能

### 注意点
一般的ではないですが、ここで使われているコードとして「addM7」があります。
7th・dim7と長7度が同時に鳴らされるときに使われることがあります。


## デモ
こちらへどうぞ: [note2chordjs.bitbucket.io](https://note2chordjs.bitbucket.io/)


## 使い方
```html
<script src="note2chord.js"></script>
```
このリポジトリーをダウンロードして、note2chord.js を所定の位置に置きます。
そして HTML にこのタグを追記してください。


## 関数
### 本体
#### 具体的なコードネーム用
```js
note2chord.names([1, 4, 8], true);
```

#### 度数でのコードネーム用
```js
note2chord.degreeNames([1, 4, 8], true, 1);
```

### 引数
- 第1引数 `notes`: Array
  - MIDI ノートのように音高が入った配列を渡してください
  - 数字の順番は揃えなくても大丈夫です
    - `[0, 4, 7]`も`[4, 0, 7]`も同じです
- 第2引数 `accidentalPositionIsRight`: Boolean
  - これを `false` にすると左側に臨時記号がつきます ("♯C")
  - `true` にすると右側に臨時記号が付きます ("C♯")
  - デフォルト値: `true`
- 第3引数 `key`: Number (`degreeNames`のみ)
  - キーを指定します
  - `0` 指定で C メジャーキーです
  - 例えば `2` を指定するとそれは D メジャーキーということになります
  - デフォルト値: `0`

### 返り値
#### 簡易的な説明
文字列が入った配列としても使えます。
```js
["Caug/F♯", "F♯m7sus2(b5)"]
```
こんな感じの配列が返ってくると見ることができます。

#### 詳しい説明
v0.5から、配列の中身を文字列ではなくObjectを返すようにしました。
Objectは`toString()`を再定義してあるので上のように文字列が入った配列としても利用可能です。

"C♯m7(b5)/F♯"というコードを例にとると、次のような形で値が格納されています。
```
object = note2chord.names([6, 13, 16, 19, 23])[0];

object      // -> "C♯m7(b5)/F♯" (※ 文字列として参照した場合)
object.name // -> "C♯m7(b5)/F♯"

// 分子のコード。分数コードでなくても基本的な部分はこちらに入る
object.numerator                                 // -> "C♯" (※ 文字列として参照した場合)
object.numerator.name                            // -> "C♯" 
object.numerator.alphabet                        // -> "C"  (アルファベットのみ格納)
object.numerator.accidental                      // -> "♯"  (臨時記号のみ格納)
object.numerator.semitoneDistanceFromDenominator // -> 7    (分子のルート C♯ と 分母のルート F♯ の距離を半音単位で格納。分数コードの場合は 0)

// 分母のコード。分数コードでない場合は全て "" となる
object.denominator            // -> "F♯" (※ 文字列として参照した場合)
object.denominator.name       // -> "F♯" 
object.denominator.alphabet   // -> "F"  (アルファベットのみ格納)
object.denominator.accidental // -> "♯"  (臨時記号のみ格納)

// コードのクオリティー、テンション、オミット
object.quality  // -> "m7" (6, m6, 7, m, m7, M7, mM7, dim7, dimM7, aug, aug7, augM7, 9, 11, 13, sus2, sus4 などが入る)
object.tension  // -> "b5" (テンションが入る)
object.omission // -> ""   (omit3, omit5, omit3omit5 が入る)
```
「※ 文字列として参照した場合」と書かれているところは`toString()`で定義されているところです。


## 使用例
### 具体的なコードネームが欲しいとき
```js
note2chord.names([1, 4, 8]);
// -> ["C♯m"]
```

### 具体的なコードネームが欲しいとき (左側に臨時記号をつけたいとき)
```js
note2chord.names([1, 4, 8], false);
// -> ["♯Cm"]
```

### 度数のコードが欲しいとき
```js
note2chord.degreeNames([2, 6, 9], true, 2);
// -> ["Ⅰ"]
```

### いくつか候補があるときはこのような感じです
```js
note2chord.names([7, 17, 20, 23, 27])
// -> ["G7omit5(b9,b13)", "Gaug7(b9)", "G♯m6/G"]
```

### Blackadder Chord も出せます
```js
note2chord.names([6, 8, 12, 16])
// -> ["Caug/F♯", "F♯m7sus2(b5)"]
```


## このコードがない！
デモを試したとき「このコードネームがない」「ここのコードが間違っている」等があれば、そのコード名とデモの下部の「デバッグ部」左側の数字を Issues に書き込んでいただけると助かります。


## 仕組み
12 音あるうち root を除いた 11 音が鳴ったり鳴らなかったりを考えると、2 ^ 11 = 2048 種類の組み合わせを考えれば良いということになります。
11 音の各音を 1 bit に見立てて、2048 要素からなるテーブルからデータを参照しています。

現実的な話、2048 種類ものコードを埋めるには相当時間がかかるのである程度のところまでしかやっていません。
埋まっていない部分は条件分岐でコードを生成していますが、穴はあります。

また、同じ構成音でも複数の名前が考えられるので、実質的には2048種類以上あります。
