/*

MIT License

Copyright (c) 2019 36kHz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

let note2chord = {
	details: (isSoundings) => {
		const number = ((arr) => {
			let num = 0;
			//for(let i=11-1; i>=0; i--){
			for(let i=1; i<12; i++){ // 1 始まりに注意
				num <<= 1;
				num |= isSoundings[i] ? 1 : 0;
			}
			return num;
		})(isSoundings);
		const rawChordName = note2chord._tables[number];
		if(rawChordName.match(/\?/)){
			return [{
				numerator: 0,
				quality: "",
				omission: "",
				tension: "",
				number: number,
				tableExists: true,
			}];
		}else if(rawChordName === ""){
			// コードの名前の付加
			let chordQuality = "";
			if(isSoundings[3] && !isSoundings[4]){ // M3 は鳴らず、m3 のみが鳴っているとき
				chordQuality += "m"
			}
			if(isSoundings[10]){ // m7 が鳴っているとき
				chordQuality += "7"
			}else if(isSoundings[11]){ // M7 が鳴っているとき
				chordQuality += "M7";
			}else if(!isSoundings[10] && !isSoundings[11]){ // m7 も M7 も鳴っていないとき
				if(isSoundings[9]){ // 6 が鳴っているとき
					chordQuality += "6";
				}
			}
			if(!isSoundings[3] && !isSoundings[4]){ // m3 も M3 も鳴っていないとき
				if(isSoundings[2]){ // 9thが鳴っている
					chordQuality += "sus2";
				}
				if(isSoundings[5]){ // 4thが鳴っている
					chordQuality += "sus4";
				}
			}
			let tensionNames = [];
			if(isSoundings[1]){ // b9 が鳴っているとき
				tensionNames.push("b9");
			}
			if(isSoundings[3] || isSoundings[4]){ // m3 か M3 が鳴っているとき
				if(isSoundings[2]){ // 9 が鳴っているとき // ■■ add9 はどうする？
					tensionNames.push("9");
				}
			}
			if(isSoundings[3] && isSoundings[4]){ // #9 も M3 も鳴っているとき
				tensionNames.push("#9");
			}
			if(isSoundings[3] || isSoundings[4]){ // m3 か M3 が鳴っているとき
				if(isSoundings[5]){ // 11 が鳴っているとき
					tensionNames.push("11");
				}
			}
			if(isSoundings[6]){ // b5 or #11 が鳴っているとき // ■■ b5, dim はどうする？
				tensionNames.push("#11");
			}
			if(isSoundings[8]){ // b13 が鳴っているとき // ■■ aug はどうする？
				tensionNames.push("b13");
			}
			if(isSoundings[9]){ // 13 が鳴っているとき
				if(isSoundings[10] || isSoundings[11]){ // m7 or M7 が鳴っているとき
					tensionNames.push("13");
				}
			}
			return [{
				numerator: 0,
				quality: chordQuality,
				omission: "",
				tension: tensionNames.join(","),
				number: number,
				tableExists: false,
			}];
		}else{
			const options = rawChordName.split(";");
			let details = [];
			for(let k=0; k<options.length; k++){
				const fractions = options[k].split("/");
				const numeratorStrings = "C#,Db,D#,Eb,F#,Gb,G#,Ab,A#,Bb,C,D,E,F,G,A,B".split(",");
				const numeratorNumbers = [1,1,3,3,6,6,8,8,10,10,0,2,4,5,7,9,11];
				let numerator = 0;
				let qualityAndTension = "";
				for(let i=0; i<17; i++){
					if(fractions[0].match(new RegExp(numeratorStrings[i]))){
						numerator = numeratorNumbers[i];
						qualityAndTension = fractions[0].slice(numeratorStrings[i].length);
						break;
					}
				}
				const [qualityAndOmission, tension] = qualityAndTension.replace(/(.*?)\((.+?)\)(.*)/, "$1$3|$2").split("|");
				const [quality, omission] = (qualityAndOmission || "").replace(/(.*?)(omit.*)/, "$1|$2").split("|");
				details.push({
					numerator: numerator,
					quality: quality || "",
					omission: omission || "",
					tension: tension || "",
					number: number,
					tableExists: true,
				});
			}
			return details;
		}
	},
	ChordClass: class{
		constructor(numerator, denominator, quality, tension, omission, accidentalPositionIsRight = true, optionalData = {}, semitoneDistanceFromDenominator){
			this.quality = quality; // 6, m6, 7, m, m7, M7, mM7, dim7, dimM7, aug, aug7, augM7, 9, 11, 13
			this.tension = tension;
			this.omission = omission;
			this.optionalData = optionalData;
			this.numerator = new (class{
				constructor(str){
					if(str){
						const strs = str.replace(/([A-GⅠ-Ⅶ])((b|#)*)/, "$1|$2").split("|");
						this.alphabet = strs[0];
						this.accidental = strs[1];
					}else{
						this.alphabet = "";
						this.accidental = "";
					}
					this.semitoneDistanceFromDenominator = semitoneDistanceFromDenominator;
					this.name = this._getName();
				}
				_getName(){
					if(accidentalPositionIsRight){
						return this.alphabet + this.accidental;
					}else{
						return this.accidental + this.alphabet;
					}
				}
				toString(){
					return this._getName();
				}
			})(numerator);
			this.denominator = new (class{
				constructor(str){
					if(str){
						const strs = str.replace(/([A-GⅠ-Ⅶ])((b|#)*)/, "$1|$2").split("|");
						this.alphabet = strs[0];
						this.accidental = strs[1];
					}else{
						this.alphabet = "";
						this.accidental = "";
					}
					this.name = this._getName();
				}
				_getName(){
					if(accidentalPositionIsRight){
						return this.alphabet + this.accidental;
					}else{
						return this.accidental + this.alphabet;
					}
				}
				toString(){
					return this._getName();
				}
			})(denominator);
			this.name = this._getName();
		}
		_getName(){
			const tension = this.tension ? "(" + this.tension + ")" : "";
			const denominator = this.denominator.toString() === "" ? "" : ("/" + this.denominator);
			return this.numerator + this.quality + this.omission + tension + denominator;
		}
		toString(){
			return this._getName();
		}
	},
	names: (notes, accidentalPositionIsRight = true) => {
		notes = notes.concat().sort((a, b) => (a < b ? -1 : 1));
		let isSoundings = [];
		for(let i=1; i<notes.length; i++){
			isSoundings[(notes[i] - notes[0] + 1200) % 12] = true;
		}
		let chords = note2chord.details(isSoundings);
		const chordRootNames = "C.C#.D.Eb.E.F.F#.G.G#.A.Bb.B".replace(/#/g, "♯").replace(/b/g, "♭").split(".");
		let names = [];
		for(let i=0; i<chords.length; i++){
			const chord = chords[i];
			const denominator = chord.numerator ? chordRootNames[(notes[0] + 1200) % 12] : "";
			const numerator = chordRootNames[(chord.numerator + notes[0] + 1200) % 12];
			const semitoneDistanceFromDenominator = chord.numerator;
			const optionalData = {number: chord.number, tableExists: chord.tableExists, };
			names.push(new note2chord.ChordClass(
				numerator,
				denominator,
				chord.quality,
				chord.tension,
				chord.omission,accidentalPositionIsRight,
				optionalData,
				semitoneDistanceFromDenominator
			));
		}
		return names;
	},
	degreeNames: (notes, accidentalPositionIsRight = true, key = 0) => {
		notes = notes.concat().sort((a, b) => (a < b ? -1 : 1));
		let isSoundings = [];
		for(let i=1; i<notes.length; i++){
			isSoundings[(notes[i] - notes[0] + 1200) % 12] = true;
		}
		let chords = note2chord.details(isSoundings);
		const chordRootNames = "Ⅰ.Ⅰ#.Ⅱ.Ⅲb.Ⅲ.Ⅳ.Ⅳ#.Ⅴ.Ⅴ#.Ⅵ.Ⅶb.Ⅶ".replace(/#/g, "♯").replace(/b/g, "♭").split(".");
		let names = [];
		for(let i=0; i<chords.length; i++){
			const chord = chords[i];
			const denominator = chord.numerator ? chordRootNames[(notes[0] - key + 1200) % 12] : "";
			const numerator = chordRootNames[(chord.numerator + notes[0] - key + 1200) % 12];
			const semitoneDistanceFromDenominator = chord.numerator;
			const optionalData = {number: chord.number, tableExists: chord.tableExists, };
			names.push(new note2chord.ChordClass(
				numerator,
				denominator,
				chord.quality,
				chord.tension,
				chord.omission,
				accidentalPositionIsRight,
				optionalData,
				semitoneDistanceFromDenominator
			));
		}
		return names;
	},
	// はじめにカーソルを置いて、そのあと Ctrl + shift + End (NumLock は解除)で一番下に移動する
	_tables: `Comit3omit5
	CM7omit3omit5
	C7omit3omit5
	C7addM7omit3omit5
	C6omit3omit5;Amomit5/C
	CM7(13)omit3omit5
	C7(13)omit3omit5
	?7
	Caugomit3;Abomit5/C
	CaugM7omit3
	Caug7omit3
	?7
	Caug(13)omit3
	
	
	?7
	Comit3
	CM7omit3
	C7omit3
	?7
	C6omit3
	
	
	?7
	C(b13)omit3
	
	
	?7
	
	
	
	?7
	C(b5)omit3
	CM7(b5)omit3
	C7(b5)omit3
	?7
	C6(b5)omit3
	
	
	?7
	Caug(#11)omit3;C(b5,b13)omit3
	
	
	?7
	
	
	
	?7
	C(#11)omit3
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	Csus4omit5;Fomit3/C
	CM7sus4omit5
	Fsus4/C;C7sus4omit5
	?7
	F/C;C6sus4omit5
	
	
	?7
	Fm/C;Caugsus4
	
	
	?7
	
	
	
	?7
	Csus4
	CM7sus4
	C7sus4
	?7
	Fadd9/C;C6sus4
	
	
	?7
	Csus4(b13)
	
	
	?7
	
	
	
	?7
	C(b5)sus4
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	Comit5
	CM7omit5
	C7omit5
	?7
	Am/C;C6omit5
	
	
	?7
	Caug
	CaugM7
	Caug7
	?7
	AmM7/C
	
	
	?7
	C
	CM7
	C7
	?7
	C6;Am7/C
	CM7(13)
	C7(13)
	?7
	C(b13);G#augM7/C
	CM7(b13)
	C7(b13)
	?7
	C6(b13)
	
	
	?7
	C(b5)
	
	C7(b5);C7(#11)omit5
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	C6(#11)
	
	
	?7
	
	
	
	?7
	C6(#11,b13)
	
	
	?7
	C(11)omit5
	
	
	?7
	FM7/C
	
	
	?7
	FmM7/C
	
	
	?7
	
	
	
	?7
	Cadd11
	
	
	?7
	C6(11)
	
	
	?7
	
	
	
	?7
	C6(11,b13)
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	C6(11,#11)
	
	
	?7
	
	
	
	?7
	C6(11,#11,b13)
	
	
	?7
	Cmomit5
	CmM7omit5
	Cm7omit5
	?7
	Am(b5)/C;Cm6omit5
	
	
	?7
	G#/C;Cmaug
	G#m/C;CmM7(b13);CmaugM7
	Abadd9/C;Cm7(b13)omit5;Cmaug7
	?7
	
	
	
	?7
	Cm
	CmM7
	Cm7
	?7
	Cm6;Am7(b5)/C
	
	
	?7
	G#M7/C;Cm(b13)
	
	
	?7
	Cm6(b13)
	
	
	?7
	Cm(b5)
	B/C;CmM7(b5);CdimM7;CmM7(#11)omit5
	Cm7(b5);Ebm6/C
	?7
	Cdim7
	B7(b9)/C
	
	?7
	G#7/C
	G#7(#9)/C;CmM7(b5,b13)
	
	?7
	G#7(b9)/C;Cdim7(b13)
	G#7(b9,#9)/C;CmM7(#5,#11,13)
	
	?7
	
	
	
	?7
	Cm6(#11)
	
	
	?7
	
	
	
	?7
	Cm6(#11,b13)
	
	
	?7
	Cm(11)omit5
	
	
	?7
	F7/C
	
	
	?7
	Fm7/C
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	Cm6(11)
	
	
	?7
	
	
	
	?7
	Cm6(11,b13)
	
	
	?7
	
	
	Cm7(b5,11);Cm7(11,#11)omit5
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	Cm6(11,#11)
	
	
	?7
	
	
	
	?7
	Cm6(11,#11,b13)
	
	
	?7
	C(#9)omit5
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	C6(#9)
	
	
	?7
	
	
	
	?7
	C6(#9,b13)
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	C6(#9,#11)
	
	
	?7
	
	
	
	?7
	C6(#9,#11,b13)
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	C6(#9,11)
	
	
	?7
	
	
	
	?7
	C6(#9,11,b13)
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	C6(#9,11,#11)
	
	
	?7
	
	
	
	?7
	C6(#9,11,#11,b13)
	
	
	?7
	Csus2omit5
	CM7sus2omit5
	C7sus2omit5
	?7
	C6sus2omit5
	
	
	?7
	Caugsus2
	
	
	?7
	
	
	
	?7
	Csus2;Gsus4/C
	G/C
	Gm/C
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	C(b5)sus2
	Bm/C
	F#aug/C;Cm7(b5)sus2
	?7
	D7/C
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	GmM7/C
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	Dmomit5/C
	
	Bb/C
	?7
	Dm7/C
	
	BbM9/C
	?7
	Dm(b5)/C
	
	Bb7/C
	?7
	
	
	
	?7
	
	G7/C;CM9sus4
	Gm7/C;C9sus4;C11omit3
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	Cadd9omit5
	
	
	?7
	
	
	
	?7
	
	
	Caug7(9);C7(9,b13)omit5
	?7
	
	
	
	?7
	Cadd9
	CM9
	C9
	?7
	C6(9)
	CM9(13)
	C9(13)
	?7
	
	CM9(b13)
	C9(b13)
	?7
	C6(9,b13)
	CM9(b13,13)
	C9(b13,13)
	?7
	
	C9(b5);CM7(9,#11)omit5
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CM9(#11)
	C9(#11)
	?7
	C6(9,#11)
	CM9(#11,13)
	C9(#11,13)
	?7
	
	CM9(#11,b13)
	C9(#11,b13)
	?7
	C6(9,#11,b13)
	CM9(#11,b13,13)
	C9(#11,b13,13)
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CM11
	C11
	?7
	C6(9,11)
	CM13
	C13
	?7
	
	CM11(b13)
	C11(b13)
	?7
	C6(9,11,b13)
	CM13(b13,13)
	C13(b13,13)
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CM11(11,#11)
	C11(11,#11)
	?7
	C6(9,11,#11)
	CM13(11,#11)
	C13(11,#11)
	?7
	
	CM11(11,#11,b13)
	C11(11,#11,b13)
	?7
	C6(9,11,#11,b13)
	CM13(11,#11,b13,13)
	C13(11,#11,b13,13)
	?7
	Cmadd9omit5
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CmM9
	Cm9
	?7
	Cm6(9)
	CmM9(13)
	Cm9(13)
	?7
	
	CmM9(b13)
	Cm9(b13)
	?7
	Cm6(9,b13)
	CmM9(b13,13)
	Cm9(b13,13)
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CmM9(#11)
	Cm9(#11)
	?7
	Cm6(9,#11)
	CmM9(#11,13)
	Cm9(#11,13)
	?7
	
	CmM9(#11,b13)
	Cm9(#11,b13)
	?7
	Cm6(9,#11,b13)
	CmM9(#11,b13,13)
	CmM9(#11,b13,13)
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CmM11
	Cm11
	?7
	Cm6(9,11)
	CmM13
	Cm13
	?7
	
	CmM11(b13)
	Cm11(b13)
	?7
	Cm6(9,11,b13)
	CmM13(b13,13)
	Cm13(b13,13)
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CmM11(11,#11)
	Cm11(11,#11)
	?7
	Cm6(9,11,#11)
	CmM13(11,#11)
	Cm13(11,#11)
	?7
	
	CmM11(11,#11,b13)
	Cm11(11,#11,b13)
	?7
	Cm6(9,11,#11,b13)
	CmM13(11,#11,b13,13)
	Cm13(11,#11,b13,13)
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CM9(9,#9)
	C9(9,#9)
	?7
	C6(9,#9)
	CM9(9,#9,13)
	C9(9,#9,13)
	?7
	
	CM9(9,#9,b13)
	C9(9,#9,b13)
	?7
	C6(9,#9,b13)
	CM9(9,#9,b13,13)
	C9(9,#9,b13,13)
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CM9(9,#9,#11)
	C9(9,#9,#11)
	?7
	C6(9,#9,#11)
	CM9(9,#9,#11,13)
	C9(9,#9,#11,13)
	?7
	
	CM9(9,#9,#11,b13)
	C9(9,#9,#11,b13)
	?7
	C6(9,#9,#11,b13)
	CM9(9,#9,#11,b13,13)
	C9(9,#9,#11,b13,13)
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CM11(9,#9)
	C11(9,#9)
	?7
	C6(9,#9,11)
	CM13(9,#9)
	C13(9,#9)
	?7
	
	CM11(9,#9,b13)
	C11(9,#9,b13)
	?7
	C6(9,#9,11,b13)
	CM13(9,#9,b13,13)
	C13(9,#9,b13,13)
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CM11(9,#9,11,#11)
	C11(9,#9,11,#11)
	?7
	C6(9,#9,11,#11)
	CM13(9,#9,11,#11)
	C13(9,#9,11,#11)
	?7
	
	CM11(9,#9,11,#11,b13)
	C11(9,#9,11,#11,b13)
	?7
	C6(9,#9,11,#11,b13)
	CM13(9,#9,11,#11,b13,13)
	C13(9,#9,11,#11,b13,13)
	D13(b9,9,#9,11,#11,b13,13)/C
	C(b9)omit3omit5
	CM7(b9)omit3omit5
	C7(b9)omit3omit5
	?7
	C6(b9)omit3omit5
	
	
	?7
	Caug(b9)omit3
	
	
	?7
	
	
	
	?7
	C(b9)omit3
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	C(b5,b9)
	
	F#/C
	?7
	F#m/C
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	C#omit5/C
	
	Bbm/C
	?7
	
	
	
	?7
	C#M7/C
	
	Bbm7/C;C7(b9,11,b13)omit3omit5
	?7
	
	
	
	?7
	
	
	Gm7(b5)/C;C7(b9,11)omit3;C7(b9)sus4
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	C#momit5/C
	
	
	?7
	A/C
	
	
	?7
	C#mM7/C;Caug(b9);C(b9,b13)omit5
	
	C7(b9,b13)omit5;Caug7(b9);C#m6/C
	?7
	
	
	
	?7
	
	
	C7(b9)
	?7
	C6(b9)
	
	
	?7
	
	
	
	?7
	C6(b9,b13)
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	C6(b9,#11)
	
	
	?7
	
	
	
	?7
	C6(b9,#11,b13)
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	C6(b9,11)
	
	
	?7
	
	
	
	?7
	C6(b9,11,b13)
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	C6(b9,11,#11)
	
	
	?7
	
	
	
	?7
	C6(b9,11,#11,b13)
	
	
	?7
	Cm(b9)omit5
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	Cm6(b9)
	
	
	?7
	
	
	
	?7
	Cm6(b9,b13)
	
	
	?7
	
	
	Cm7(b5,b9);Cm7(b9,#11)omit5
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	Cm6(b9,#11)
	
	
	?7
	
	
	
	?7
	Cm6(b9,#11,b13)
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	Cm6(b9,11)
	
	
	?7
	
	
	
	?7
	Cm6(b9,11,b13)
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	Cm6(b9,11,#11)
	
	
	?7
	
	
	
	?7
	Cm6(b9,11,#11,b13)
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	C6(b9,#9)
	
	
	?7
	
	
	
	?7
	C6(b9,#9,b13)
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	C6(b9,#9,#11)
	
	
	?7
	
	
	
	?7
	C6(b9,#9,#11,b13)
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	C6(b9,#9,11)
	
	
	?7
	
	
	
	?7
	C6(b9,#9,11,b13)
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	C6(b9,#9,11,#11)
	
	
	?7
	
	
	
	?7
	C6(b9,#9,11,#11,b13)
	CM7(b9,#9,11,#11,b13,13)
	C7(b9,#9,11,#11,b13,13)
	Eb13(b9,9,#9,11,#11,b13,13)/C
	Csus2(b9)omit5
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CM9(b9,9)
	C9(b9,9)
	?7
	C6(b9,9)
	CM9(b9,9,13)
	C9(b9,9,13)
	?7
	
	CM9(b9,9,b13)
	C9(b9,9,b13)
	?7
	C6(b9,9,b13)
	CM9(b9,9,b13,13)
	C9(b9,9,b13,13)
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CM9(b9,9,#11)
	C9(b9,9,#11)
	?7
	C6(b9,9,#11)
	CM9(b9,9,#11,13)
	C9(b9,9,#11,13)
	?7
	
	CM9(b9,9,#11,b13)
	C9(b9,9,#11,b13)
	?7
	C6(b9,9,#11,b13)
	CM9(b9,9,#11,b13,13)
	C9(b9,9,#11,b13,13)
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CM11(b9,9)
	C11(b9,9)
	?7
	C6(b9,9,11)
	CM13(b9,9)
	C13(b9,9)
	?7
	
	CM11(b9,9,b13)
	C11(b9,9,b13)
	?7
	C6(b9,9,11,b13)
	CM13(b9,9,b13,13)
	C13(b9,9,b13,13)
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CM11(b9,9,11,#11)
	C11(b9,9,11,#11)
	?7
	C6(b9,9,11,#11)
	CM13(b9,9,11,#11)
	C13(b9,9,11,#11)
	?7
	
	CM11(b9,9,11,#11,b13)
	C11(b9,9,11,#11,b13)
	?7
	C6(b9,9,11,#11,b13)
	CM13(b9,9,11,#11,b13,13)
	C13(b9,9,11,#11,b13,13)
	E13(b9,9,#9,11,#11,b13,13)/C
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CmM9(b9,9)
	CmM9(b9,9)
	?7
	Cm6(b9,9)
	CmM9(b9,9,13)
	CmM9(b9,9,13)
	?7
	
	CmM9(b9,9,b13)
	CmM9(b9,9,b13)
	?7
	Cm6(b9,9,b13)
	CmM9(b9,9,b13,13)
	CmM9(b9,9,b13,13)
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CmM9(b9,9,#11)
	CmM9(b9,9,#11)
	?7
	Cm6(b9,9,#11)
	CmM9(b9,9,#11,13)
	CmM9(b9,9,#11,13)
	?7
	
	CmM9(b9,9,#11,b13)
	CmM9(b9,9,#11,b13)
	?7
	Cm6(b9,9,#11,b13)
	CmM9(b9,9,#11,b13,13)
	CmM9(b9,9,#11,b13,13)
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CmM11(b9,9)
	Cm11(b9,9)
	?7
	Cm6(b9,9,11)
	CmM13(b9,9)
	Cm13(b9,9)
	?7
	
	CmM11(b9,9,b13)
	Cm11(b9,9,b13)
	?7
	Cm6(b9,9,11,b13)
	CmM13(b9,9,b13,13)
	Cm13(b9,9,b13,13)
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CmM11(b9,9,11,#11)
	Cm11(b9,9,11,#11)
	?7
	Cm6(b9,9,11,#11)
	CmM13(b9,9,11,#11)
	Cm13(b9,9,11,#11)
	?7
	
	CmM11(b9,9,11,#11,b13)
	Cm11(b9,9,11,#11,b13)
	?7
	Cm6(b9,9,11,#11,b13)
	CmM13(b9,9,11,#11,b13,13)
	Cm13(b9,9,11,#11,b13,13)
	F13(b9,9,#9,11,#11,b13,13)/C
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CM9(b9,9,#9)
	C9(b9,9,#9)
	?7
	C6(b9,9,#9)
	CM9(b9,9,#9,13)
	C9(b9,9,#9,13)
	?7
	
	CM9(b9,9,#9,b13)
	C9(b9,9,#9,b13)
	?7
	C6(b9,9,#9,b13)
	CM9(b9,9,#9,b13,13)
	C9(b9,9,#9,b13,13)
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CM9(b9,9,#9,#11)
	C9(b9,9,#9,#11)
	?7
	C6(b9,9,#9,#11)
	CM9(b9,9,#9,#11,13)
	C9(b9,9,#9,#11,13)
	?7
	
	CM9(b9,9,#9,#11,b13)
	C9(b9,9,#9,#11,b13)
	?7
	C6(b9,9,#9,#11,b13)
	CM9(b9,9,#9,#11,b13,13)
	C9(b9,9,#9,#11,b13,13)
	F#13(b9,9,#9,11,#11,b13,13)/C
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CM11(b9,9,#9)
	C11(b9,9,#9)
	?7
	C6(b9,9,#9,11)
	CM13(b9,9,#9)
	C13(b9,9,#9)
	?7
	
	CM11(b9,9,#9,b13)
	C11(b9,9,#9,b13)
	?7
	C6(b9,9,#9,11,b13)
	CM13(b9,9,#9,b13,13)
	C13(b9,9,#9,b13,13)
	G13(b9,9,#9,11,#11,b13,13)/C
	
	
	
	?7
	
	
	
	?7
	
	
	
	?7
	
	CM13(b9,#9,#11,b13)omit5
	C13(b9,#9,#11,b13)omit5
	G#13(b9,9,#9,11,#11,b13,13)/C
	
	CM11(b9,9,#9,11,#11)
	C11(b9,9,#9,11,#11)
	?7
	C6(b9,9,#9,11,#11)
	CM13(b9,9,#9,11,#11)
	C13(b9,9,#9,11,#11)
	A13(b9,9,#9,11,#11,b13,13)/C
	
	CM11(b9,9,#9,11,#11,b13)
	C11(b9,9,#9,11,#11,b13)
	Bb13(b9,9,#9,11,#11,b13,13)/C
	C6(b9,9,#9,11,#11,b13)
	CM13(b9,9,#9,11,#11,b13,13)
	C13(b9,9,#9,11,#11,b13,13)
	C#13(b9,9,#9,11,#11,b13,13)/C`
	.replace(/\t/g, "").split("\n"),
};
